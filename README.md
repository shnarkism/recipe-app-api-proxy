# recipe-app-api-proxy

# recipe api proxy application

NGINX proxy app for our recipe api

##USAGE

### ENVIRONMENT VARIABLES

* `LISTEN_PORT` - Port to listen (default: '8000')
* `APP HOST` - Hostname of the app to forward requests to (default: 'app')
* `APP_PORT` - Port of the app to forward requests to (default: '9000')